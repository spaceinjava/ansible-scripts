#!/bin/bash

service=$1
version=$2
env=$3

#cd docker-compose
export VERSION=$version

aws ecr get-login-password --region ap-south-1 | docker login --username AWS --password-stdin 304752749271.dkr.ecr.ap-south-1.amazonaws.com

echo "Deploying $service with version $version"
docker-compose -f docker-compose.yaml -f docker-compose-dev.yaml up -d $service-$env

docker image prune --all -f
